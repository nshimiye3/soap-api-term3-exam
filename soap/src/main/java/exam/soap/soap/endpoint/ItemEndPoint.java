package main.java.exam.soap.soap.endpoint;



import main.java.exam.soap.soap.bean.Supplier;
import main.java.exam.soap.soap.bean.Item;
import main.java.exam.soap.repository.ISupplierRepository;
import main.java.exam.soap.repository.IItemRepository;

import main.java.exam.soap.soap.repository.ISupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;

@Endpoint
public class ItemEndPoint {


    @Autowired
    private IItemRepository itemRepository;

    @Autowired
    private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://stocksoapapi.edu.com/items",localPart = "GetItemDetails")
    @ResponsePayload
    public GetItemDetailsResponse findItem(@RequestPayload GetItemDetailsRequest request)
    {
        Item item= itemRepository.findById(request.getId()).get();
        GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
        return itemDetailsResponse;
    }



    @PayloadRoot(namespace = "http://stocksoapapi.edu.com/items",localPart = "GetAllItemDetailsRequest")
    @ResponsePayload

    public GetAllItemDetailsResponse getAllItems(@RequestPayload GetAllItemDetailsRequest request)
    {
        GetAllItemDetailsResponse itemResp = new GetAllItemDetailsResponse();
        System.out.println("Reached here");
        List<Item> items = itemRepository.findAll();
        System.out.println("List: "+ items);

        for(Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
            itemResp.getItemDetails().add(itemDetailsResponse.getItemDetails());
        }

        return  itemResp;
    }



    @PayloadRoot(namespace = "http://stocksoapapi.edu.com/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        Item item = itemRepository.findById(request.getItemDetails().getSupplierId()).get();

        Item testItem = itemRepository.save(new Item(
                request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getitemCode(),
                request.getItemDetails().getstatus(),
                request.getItemDetails().getprice(),
                supplier
        ));

        System.out.println("Test: "+testItem);

//        System.out.println("course details "+ course);
        CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
        itemDetailsResponse.setItemDetails(request.getItemDetails());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }


    @PayloadRoot(namespace = "http://stocksoapapi.edu.com/items", localPart = "DeleteItemDetailsResponse")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById(request.getId());
        DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }



    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails details = mapStudent(item);
        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();
        itemDetailsResponse.setItemDetails(details);
        return itemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapUpdateItemDetails(Item std, String message) {
        ItemDetails details = mapItem(std);
        UpdateItemDetailsResponse resp = new UpdateItemDetailsResponse();
        resp.setItemDetails(details);
        resp.setMessage(message);
        return resp;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails stdDetails = new ItemDetails();
        stdDetails.setName(item.getName());
        stdDetails.setId(item.getId());
        stdDetails.setItemCode(item.getItemCode());
        stdDetails.setStatus(item.getStatus());
        stdDetails.setSupplierId(item.getSupplier().getId());
        return stdDetails;
    }
}
