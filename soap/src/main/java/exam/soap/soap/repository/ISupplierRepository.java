package main.java.exam.soap.soap.repository;

import main.java.exam.soap.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISupplierRepository extends JpaRepository<Supplier, Integer> {
}
