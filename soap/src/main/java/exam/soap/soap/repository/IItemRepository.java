package main.java.exam.soap.soap.repository;

import main.java.exam.soap.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemRepository extends JpaRepository<Item,Long>{

}
